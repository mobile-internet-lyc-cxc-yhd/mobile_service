package com.cxc.mobile_server.Mapper;

import com.cxc.mobile_server.Bean.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentMapper extends JpaRepository<Comment,String> {

    Comment findByCommentId(String commentId);

    int countCommentByToId(String articleId);

    List<Comment> findByToId(String toId);

    Page<Comment> findByToId(String toId, Pageable pageable);

    List<Comment> findByToUserIdOrderByCreateDateDesc(String toUserId);
}
