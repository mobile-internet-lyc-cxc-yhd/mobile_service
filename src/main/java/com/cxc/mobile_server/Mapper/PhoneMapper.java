package com.cxc.mobile_server.Mapper;

import com.cxc.mobile_server.Bean.Phone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhoneMapper extends JpaRepository<Phone,String> {

    List<Phone> findAll();

    Page<Phone> findByBrand(String brand, Pageable pageable);

    Page<Phone> findByPriceBetween(String left,String right,Pageable pageable);

    List<Phone> findByName(String name);

    Phone findByPhoneId(String phoneId);
}
