package com.cxc.mobile_server.Mapper;

import com.cxc.mobile_server.Bean.Liking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikingMapper extends JpaRepository<Liking,String> {

    Liking findByUserIdAndToId(String userId,String toId);

    List<Liking> findByToUserIdOrderByCreatedDateDesc(String toUserId);

    int countByToId(String toId);
}
