package com.cxc.mobile_server.Mapper;

import com.cxc.mobile_server.Bean.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserMapper extends JpaRepository<User,String> {

    User findByEmail(String email);

    User findByEmailAndPassword(String email, String password);

    User findByUserId(String userId);
}
