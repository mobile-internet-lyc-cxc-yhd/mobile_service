package com.cxc.mobile_server.Auth;

import com.cxc.mobile_server.Bean.User;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserContainer {


    private Map<String, User> users;

    public UserContainer(){
        users = new HashMap<>();
    }

    public String put(User user){
        String sessionId = UUID.randomUUID().toString().replace("-","");
        users.put(sessionId,user);
        return sessionId;
    }


    public User get(String sessionId){
        return users.get(sessionId);
    }
}
