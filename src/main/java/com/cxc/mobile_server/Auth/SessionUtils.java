package com.cxc.mobile_server.Auth;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Service
public class SessionUtils {

    private final static String SESSION_ID = "sessionId";

    @Resource
    private HttpServletRequest request;
    @Resource
    private HttpServletResponse response;


    public String getSessionId(){
        Cookie[] cookies = request.getCookies();
        if (cookies == null)
            return null;
        String sessionId = null;
        for (Cookie cookie : cookies){
            if(cookie.getName().equals(SESSION_ID)){
                sessionId = cookie.getValue();
            }
        }
        return sessionId;
    }

    public void setSessionId(String sessionId){
        Cookie cookie = new Cookie(SESSION_ID,sessionId);
        cookie.setPath("/");
        response.addCookie(cookie);
    }
}
