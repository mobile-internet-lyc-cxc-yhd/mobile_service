package com.cxc.mobile_server.Auth;

import com.cxc.mobile_server.Bean.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class Authenticator {

    private UserContainer userContainer;

    @Resource
    private SessionUtils sessionUtils;

    public Authenticator(){
        userContainer = new UserContainer();
    }

    public boolean isLogin(){
        if (userContainer.get(sessionUtils.getSessionId()) == null)
            return false;
        else
            return true;
    }

    public void login(User user){
        if (isLogin())
            return;
        String sessionId = userContainer.put(user);
        sessionUtils.setSessionId(sessionId);
    }

    public User getUser() throws UnAuthException {
        if (!isLogin())
            throw new UnAuthException();
        return userContainer.get(sessionUtils.getSessionId());
    }
}
