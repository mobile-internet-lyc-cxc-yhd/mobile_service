package com.cxc.mobile_server.Bean;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String commentId;

    private String UserId; //评论用户id
    private String toUserId; //被评论的用户的id
    private String toId; //评论对象的id
    private String content; // 评论内容
    private Date createDate; // 评论时间
    @Transient
    private String nickName;
    @Transient
    private int likingNum; //点赞数
    @Transient
    private boolean isLiking; // 自己是否点赞

    public int getLikingNum() {
        return likingNum;
    }

    public void setLikingNum(int likingNum) {
        this.likingNum = likingNum;
    }

    public boolean isLiking() {
        return isLiking;
    }

    public void setLiking(boolean liking) {
        isLiking = liking;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    @Transient
    private List<Comment> commentList;

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
