package com.cxc.mobile_server.Bean;

import java.util.Map;

public class ReturnInfo {
    private String code;
    private Object data;

    public ReturnInfo(String code, Object data) {
        this.code = code;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static ReturnInfo success(Object data){
        return new ReturnInfo("0",data);
    }

    public static ReturnInfo error(Object data){
        return new ReturnInfo("1",data);
    }

    public static ReturnInfo info(Map<String,String> data){
        if (data.size() > 0){
            return error(data);
        }else {
            return success(data);
        }
    }
}