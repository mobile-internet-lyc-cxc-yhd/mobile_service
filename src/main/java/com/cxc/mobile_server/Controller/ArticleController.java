package com.cxc.mobile_server.Controller;


import com.cxc.mobile_server.Bean.ReturnInfo;
import com.cxc.mobile_server.Service.ArticleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/*
 * 文章管理
 */
@RestController
@RequestMapping(value = "/article")
public class ArticleController {

    @Resource
    private ArticleService articleService;

    /*
     * 获取文章列表
     * page:页数 (>0)
     */
    @RequestMapping(value = "/list/{page}",method = RequestMethod.GET)
    public ReturnInfo list(@PathVariable int page){
        return ReturnInfo.success(articleService.getArticles(page));
    }

    /*
     * 轮播图推荐
     */
    @RequestMapping(value = "/recommend",method = RequestMethod.GET)
    public ReturnInfo recommend(){
        return ReturnInfo.success(articleService.recommend());
    }
}
