package com.cxc.mobile_server.Controller;


import com.cxc.mobile_server.Auth.Authenticator;
import com.cxc.mobile_server.Auth.UnAuthException;
import com.cxc.mobile_server.Bean.ReturnInfo;
import com.cxc.mobile_server.Service.CommentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/*
 * 评论管理
 */
@RestController
@RequestMapping(value = "/comment")
public class CommentController {

    @Resource
    private CommentService commentService;
    @Resource
    private Authenticator authenticator;

    /*
     * 评论文章
     * articleId:文章id
     * content:评论的内容
     */
    @RequestMapping(value = "/article",method = RequestMethod.POST)
    public ReturnInfo commentArt(@RequestParam(value = "articleId",defaultValue = "")String articleId,
                                 @RequestParam(value = "content",defaultValue = "")String content) throws UnAuthException {
        String userId = authenticator.getUser().getUserId();
        Map<String,String> error = commentService.commentArticle(userId,articleId,content);
        return ReturnInfo.info(error);
    }

    /*
     * 评论评论
     * commentId:要评论的评论id
     * toUserId:要评论的评论的发布者的id
     * content:评论的内容
     */
    @RequestMapping(value = "/com",method = RequestMethod.POST)
    public ReturnInfo commentCom(@RequestParam(value = "commentId",defaultValue = "")String commentId,
                                 @RequestParam(value = "content",defaultValue = "")String content,
                                 @RequestParam(value = "toUserId",defaultValue = "")String toUserId) throws UnAuthException {
        String userId = authenticator.getUser().getUserId();
        Map<String,String> error = commentService.commentCom(userId,toUserId,commentId,content);
        return ReturnInfo.info(error);
    }

    /*
     * 评论手机
     * phoneId:手机id
     * content:评论的内容
     */
    @RequestMapping(value = "/phone",method = RequestMethod.POST)
    public ReturnInfo commentPhone(@RequestParam(value = "phoneId",defaultValue = "")String phoneId,
                                   @RequestParam(value = "content",defaultValue = "")String content) throws UnAuthException {
        String userId = authenticator.getUser().getUserId();
        Map<String,String> error = commentService.commentPhone(userId,phoneId,content);
        return ReturnInfo.info(error);
    }

    /*
     * 文章下的评论列表
     * articleId:文章的id
     * page：页数
     */
    @RequestMapping(value = "/artList",method = RequestMethod.POST)
    public ReturnInfo artList(@RequestParam(value = "articleId",defaultValue = "")String articleId,
                              @RequestParam(value = "page")int page){
        return ReturnInfo.success(commentService.artList(articleId,page));
    }

    /*
     * 手机下的评论列表
     * phoneId:手机的id
     * page：页数
     */
    @RequestMapping(value = "/phoList}",method = RequestMethod.GET)
    public ReturnInfo phoList(@RequestParam(value = "phoneId",defaultValue = "")String phoneId,
                              @RequestParam(value = "page")int page){
        return ReturnInfo.success(commentService.phoList(phoneId,page));
    }


    /*
     * 评论我的评论列表
     */
    @RequestMapping(value = "/commentMeList",method = RequestMethod.GET)
    public ReturnInfo commentMeList() throws UnAuthException {
        String userId = authenticator.getUser().getUserId();
        return ReturnInfo.success(commentService.commentMeList(userId));
    }
}
