package com.cxc.mobile_server.Controller;


import com.cxc.mobile_server.Bean.Phone;
import com.cxc.mobile_server.Bean.ReturnInfo;
import com.cxc.mobile_server.Service.PhoneService;
import com.cxc.mobile_server.Service.RankService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/*
 * 手机管理
 */
@RestController
@RequestMapping(value = "/phone")
public class PhoneController {

    @Resource
    private RankService rankService;

    @Resource
    private PhoneService phoneService;

    /*
     * 手机排行榜
     */
    @RequestMapping(value = "/rank",method = RequestMethod.GET)
    public ReturnInfo rank(){
        return ReturnInfo.success(rankService.getRank());
    }

    /*
     * 通过手机品牌获取手机列表
     * brand:手机品牌
     * page:页数
     */
    @RequestMapping(value = "/searchBrand",method = RequestMethod.POST)
    public ReturnInfo searchBrand(@RequestParam(value = "brand",defaultValue = "")String brand,
                            @RequestParam(value = "page")int page){
        List<Phone> phones = phoneService.search(0,brand,page);
        return ReturnInfo.success(phones);
    }

    /*
     * 通过手机价格获取手机列表
     * page:页数
     */
    @RequestMapping(value = "/searchPrice",method = RequestMethod.POST)
    public ReturnInfo searchPrice(@RequestParam(value = "price",defaultValue = "")String price,
                                  @RequestParam(value = "page")int page){
        List<Phone> phones = phoneService.search(1,price,page);
        return ReturnInfo.success(phones);
    }

    /*
     * 获取手机品牌列表(包括图标)
     */
    @RequestMapping(value = "/brands",method = RequestMethod.GET)
    public ReturnInfo brands(){
        return ReturnInfo.success(phoneService.getBrands());
    }

    /*
     * 手机详情
     * name：手机名
     */
    @RequestMapping(value = "/detail",method = RequestMethod.POST)
    public ReturnInfo name(@RequestParam(value = "name",defaultValue = "")String name){
        List<Phone> phoneList = phoneService.search(2,name,1);
        return ReturnInfo.success(phoneList.get(0));
    }
}
