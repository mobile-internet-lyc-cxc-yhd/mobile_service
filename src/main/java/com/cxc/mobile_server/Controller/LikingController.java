package com.cxc.mobile_server.Controller;


import com.cxc.mobile_server.Auth.Authenticator;
import com.cxc.mobile_server.Auth.UnAuthException;
import com.cxc.mobile_server.Bean.ReturnInfo;
import com.cxc.mobile_server.Service.LikingService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;


/*
 * 点赞管理
 */
@RestController
@RequestMapping(value = "/liking")
public class LikingController {

    @Resource
    private Authenticator authenticator;

    @Resource
    private LikingService likingService;

    /*
     * 新建点赞
     * toId；点赞的评论的id
     * toUserId：点赞的评论的发布着id
     */
    @RequestMapping(value = "/newLiking",method = RequestMethod.POST)
    public ReturnInfo newLiking(@RequestParam(value = "toUserId",defaultValue = "")String toUserId,
                                @RequestParam(value = "toId",defaultValue = "")String toId) throws UnAuthException {
        String userId = authenticator.getUser().getUserId();
        Map<String,String> error = likingService.newLiking(userId,toUserId,toId);
        return ReturnInfo.info(error);
    }

    /*
     * 点赞我的点赞列表
     */
    @RequestMapping(value = "/likingMeList",method = RequestMethod.GET)
    public ReturnInfo likingMeList() throws UnAuthException {
        String userId = authenticator.getUser().getUserId();
        return ReturnInfo.success(likingService.LikingMeList(userId));
    }

}
