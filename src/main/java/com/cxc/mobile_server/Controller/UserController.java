package com.cxc.mobile_server.Controller;

import com.cxc.mobile_server.Bean.ReturnInfo;
import com.cxc.mobile_server.Bean.User;
import com.cxc.mobile_server.Service.RegisterService;
import com.cxc.mobile_server.Service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;


/*
 * 用户管理
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Resource
    private UserService userService;
    @Resource
    private RegisterService registerService;


    /*
     * 获取邮箱验证码
     * email:邮箱号
     */
    @RequestMapping(value = "/getCode/{email}",method = RequestMethod.GET)
    public ReturnInfo getCode(@PathVariable(value = "email")String email){
        registerService.newRegister(email);
        return ReturnInfo.success(null);
    }

    /*
     * 用户注册
     * user:用户信息
     */
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public ReturnInfo register(@RequestBody User user){
        Map<String,String> error = userService.register(user);
        return ReturnInfo.info(error);
    }

    /*
     * 用户登录
     * email:邮箱
     * password:密码
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ReturnInfo login(@RequestParam(value = "email",defaultValue = "")String email,
                            @RequestParam(value = "password",defaultValue = "")String password){
        if (userService.login(email,password)){
            return ReturnInfo.success("登录成功");
        }else {
            return ReturnInfo.error("账号密码错误");
        }
    }

}
