package com.cxc.mobile_server.Service;

import com.cxc.mobile_server.Bean.Phone;

import java.util.List;

public interface PhoneService {

    /*
     * 初始化品牌
     */
    void initBrand();

    /*
     * 获取品牌列表
     */
    List getBrands();

    /*
     * 搜索手机
     * searchTye:搜索类型
     * searchValue:搜索内容
     * page:分页
     */
    List<Phone> search(int searchType,String searchValue,int page);


    /*
     * 判断手机是否存在
     */
    boolean isExist(String phoneId);
}
