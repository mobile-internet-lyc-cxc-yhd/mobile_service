package com.cxc.mobile_server.Service;

import com.cxc.mobile_server.Bean.User;

import java.util.Map;

public interface UserService {

    /*
     * 注册用户
     */
    Map<String,String> register(User user);

    /*
     * 登录
     */
    boolean login(String email, String password);

    /*
     * 用户是否存在
     */
    boolean isExist(String userId);

    /*
     * 获取呢称
     */
    String nickName(String userId);
}
