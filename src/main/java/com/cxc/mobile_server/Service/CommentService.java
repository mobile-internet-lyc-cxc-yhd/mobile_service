package com.cxc.mobile_server.Service;

import com.cxc.mobile_server.Bean.Comment;

import java.util.List;
import java.util.Map;

public interface CommentService {

    /*
     * 评论文章
     * userId:评论者id
     * toId:评论的文章的id
     * content:评论的内容
     */
    Map<String,String> commentArticle(String userId, String toId, String content);

    /*
     * 评论评论
     * userId:评论者id
     * toId:被评论的评论的id
     * content:评论的内容
     */
     Map<String,String> commentCom(String userId, String toUserId, String toId,String content);

     /*
      * 评论手机
      * userId:评论者id
      * toId:评论的手机id
      * content：评论的内容
      */
    Map<String,String> commentPhone(String userId, String toId,String content);

    /*
     * 文章的评论数
     * articleId:文章的id
     */
    int commentNum(String articleId);

    /*
     * 是否存在
     */
    boolean isExist(String commentId);

    /*
     * 文章的评论列表
     */
    List<Comment> artList(String articleId,int page);

    /*
     * 手机评论列表
     */
    List<Comment> phoList(String phoneId,int page);

    /*
     * 评论我的列表
     */
    List<Comment> commentMeList(String userId);
}
