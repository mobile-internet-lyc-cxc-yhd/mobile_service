package com.cxc.mobile_server.Service;

public interface EmailService {

    /*
     * 发送文字邮件
     * subject:主题
     * content:内容
     * from:源邮箱地址
     * to:目的邮箱地址
     */
    void sendSimpleEmail(String from, String to,String subject, String content);
}
