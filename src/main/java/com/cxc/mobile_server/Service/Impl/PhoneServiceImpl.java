package com.cxc.mobile_server.Service.Impl;

import com.cxc.mobile_server.Bean.Phone;
import com.cxc.mobile_server.Mapper.PhoneMapper;
import com.cxc.mobile_server.Service.PhoneService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class PhoneServiceImpl implements PhoneService {

    @Resource
    private PhoneMapper phoneMapper;

    String[] brands = {
            "AGM","Google","HTC","iQOO","LG","Moto","NZONE","OPPO","realme","Redmi","ROG","U-MAGIC",
            "VERTU","vivo","一加","三星","中兴","努比亚","华为","小米","索尼","联想","苹果","荣耀","诺基亚", "酷比",
            "酷派","金立","魅族","黑鲨"
    };

    List<Map<String,String>> brandList = new ArrayList<>();

    @Override
    public void initBrand() {
        List<Phone> phoneList = phoneMapper.findAll();
        for (Phone phone : phoneList){
            for (String brand : brands){
                if (phone.getName().contains(brand)){
                    phone.setBrand(brand);
                }
            }
        }
        phoneMapper.saveAll(phoneList);
    }

    @Override
    public List getBrands() {
        if (brandList.size() != 0) return brandList;

        for (String brand : brands){
            Map<String,String> m = new HashMap<>();
            m.put("name",brand);
            m.put("picture","http://localhost:8080/file/" + brand + ".png");
            brandList.add(m);
        }
        return brandList;
    }

    @Override
    public List<Phone> search(int searchType, String searchValue, int page) {
        Pageable pageable = PageRequest.of(page-1,10);
        Page<Phone> ret = null;
        if (searchType == 0){ //通过品牌搜索
            ret = phoneMapper.findByBrand(searchValue,pageable);
        }else if (searchType == 1){ //通过价格
            int price = Integer.parseInt(searchValue);
            ret = phoneMapper.findByPriceBetween(price+"",price+1000+"",pageable);
        }else if (searchType == 2){ //通过收名
            return phoneMapper.findByName(searchValue);
        }
        return ret.getContent();
    }

    @Override
    public boolean isExist(String phoneId) {
        return phoneMapper.findByPhoneId(phoneId) != null;
    }


}
