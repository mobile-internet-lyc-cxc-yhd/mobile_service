package com.cxc.mobile_server.Service.Impl;

import com.cxc.mobile_server.Service.ArticleService;
import com.cxc.mobile_server.Service.CommentService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl implements ArticleService {

    // 缓存文章，通过页索引
    private Map<Integer,List<Article>> catchArticles = new HashMap<>();

    // 通过id索引
    private Map<String,Article> catchById = new HashMap<>();

    @Resource
    private CommentService commentService;

    private void catchArticle(int page){

        // 如果有缓存，直接返回
        if (catchArticles.get(page) != null) return;

        List<Article> list = new ArrayList<>();
        String url = "https://www.antutu.com/doc/list_" + page + ".htm";
        try {
            Document document = Jsoup.connect(url).get();
            Elements elements = document.getElementsByClass("index-c1 row");
            for (Element element : elements){
                // 解析网页
                String articleUrl = element.getElementsByTag("a").attr("href");
                String articleImg = element.getElementsByClass("bgimg").attr("style");
                String articleTitle = element.getElementsByTag("h3").text();
                String articleIntroduce = element.getElementsByTag("p").first().text();

                // 解析id
                String articleId = articleUrl.split("/")[2].split("\\.")[0];

                articleUrl = "https://www.antutu.com" + articleUrl;

                // 解析图片
                articleImg = articleImg.split("\\(")[1].split("\\)")[0];
                articleImg = articleImg.substring(1,articleImg.length()-1);

                // 生成对象
                Article article = new Article(articleId,articleUrl,articleTitle,articleIntroduce,articleImg);

                // 设置评论数，从数据库中取
                article.setCommentNum(commentService.commentNum(articleId));

                // 添加
                list.add(article);

                //缓存
                catchById.put(articleId,article);
            }

            // 缓存
            catchArticles.put(page,list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List getArticles(int pageNum) {
        if (pageNum < 1) return null;

        // 如果缓存中没有，先缓存
        catchArticle(pageNum);

        return catchArticles.get(pageNum);
    }

    @Override
    public boolean isExist(String id) {
        return catchById.containsKey(id);
    }

    @Override
    public void increaseComment(String id) {
        Article article = catchById.get(id);
        if (article == null) return;
        article.setCommentNum(article.getCommentNum()+1);
    }

    @Override
    public List recommend() {
        // 随机推荐所在的页数
        int randomNum = (int) (Math.random() * 100);
        // 缓存文章
        catchArticle(randomNum);
        // 推荐页中前5篇文章
        return catchArticles.get(randomNum).subList(0,5);
    }

    static class Article{
        private String articleId;
        private String url;
        private String title;
        private String introduce;
        private String img;
        private int commentNum;

        public Article(String articleId, String url, String title, String introduce, String img) {
            this.articleId = articleId;
            this.url = url;
            this.title = title;
            this.introduce = introduce;
            this.img = img;
        }

        public int getCommentNum() {
            return commentNum;
        }

        public void setCommentNum(int commentNum) {
            this.commentNum = commentNum;
        }

        public String getArticleId() {
            return articleId;
        }

        public void setArticleId(String articleId) {
            this.articleId = articleId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIntroduce() {
            return introduce;
        }

        public void setIntroduce(String introduce) {
            this.introduce = introduce;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }
}
