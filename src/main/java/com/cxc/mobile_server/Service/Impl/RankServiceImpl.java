package com.cxc.mobile_server.Service.Impl;

import com.cxc.mobile_server.Service.RankService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RankServiceImpl implements RankService {

    private List<Phone> phones = null;

    private void newRank() {
        phones = new ArrayList<>();

        try {
            Document document = Jsoup.connect("https://www.antutu.com/ranking/rank1.htm").get();
            Elements lists = document.getElementsByClass("nrank-b");
            for (Element list : lists){
                Elements items = list.getElementsByTag("li");
                String rank = items.first().getElementsByTag("span").first().text();
                String[] values = new String[6];
                int i = 0;
                for (Element item : items){
                    values[i++] = item.ownText();
                }
                Phone phone = new Phone(rank,
                        values[0],
                        values[1],
                        values[2],
                        values[3],
                        values[4],
                        values[5]);
                phones.add(phone);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List getRank() {
        if (phones == null) newRank();
        return phones;
    }


    static class Phone{
        private String rank;
        private String name;
        private String cup;
        private String gpu;
        private String mem;
        private String ux;
        private String score;

        public Phone(String rank, String name, String cup, String gpu, String mem, String ux, String score) {
            this.rank = rank;
            this.name = name;
            this.cup = cup;
            this.gpu = gpu;
            this.mem = mem;
            this.ux = ux;
            this.score = score;
        }

        @Override
        public String toString() {
            return "Phone{" +
                    "rank='" + rank + '\'' +
                    ", name='" + name + '\'' +
                    ", cup='" + cup + '\'' +
                    ", gpu='" + gpu + '\'' +
                    ", mem='" + mem + '\'' +
                    ", ux='" + ux + '\'' +
                    ", score='" + score + '\'' +
                    '}';
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCup() {
            return cup;
        }

        public void setCup(String cup) {
            this.cup = cup;
        }

        public String getGpu() {
            return gpu;
        }

        public void setGpu(String gpu) {
            this.gpu = gpu;
        }

        public String getMem() {
            return mem;
        }

        public void setMem(String mem) {
            this.mem = mem;
        }

        public String getUx() {
            return ux;
        }

        public void setUx(String ux) {
            this.ux = ux;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }
    }

}
