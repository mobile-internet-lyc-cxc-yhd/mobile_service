package com.cxc.mobile_server.Service.Impl;

import com.cxc.mobile_server.Service.EmailService;
import com.cxc.mobile_server.Service.RegisterService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class RegisterServiceImpl implements RegisterService {

    private Map<String,String> catchCodes = new HashMap<>();

    @Resource
    private EmailService emailService;

    @Override
    public void newRegister(String email) {
        //生成验证码
        String code = buildCode();
        //发邮件
        emailService.sendSimpleEmail("370826261@qq.com",email,"验证码",code);
        //添加缓存
        catchCodes.put(email,code);
    }


    @Override
    public boolean verifyCode(String email, String code) {
        String catchCode = catchCodes.get(email);

        if (catchCode == null) return false;
        if (!catchCode.equals(code)) return false;

        //清缓存
        catchCodes.remove(email);
        return true;
    }

    private String buildCode(){
        String code = "";
        for (int i=0;i<6;i++){
            code += (int)(Math.random()*10);
        }
        return code;
    }
}
