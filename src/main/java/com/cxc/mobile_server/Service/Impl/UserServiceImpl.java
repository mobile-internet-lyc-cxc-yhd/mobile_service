package com.cxc.mobile_server.Service.Impl;

import com.cxc.mobile_server.Auth.Authenticator;
import com.cxc.mobile_server.Bean.User;
import com.cxc.mobile_server.Mapper.UserMapper;
import com.cxc.mobile_server.Service.RegisterService;
import com.cxc.mobile_server.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private RegisterService registerService;
    @Autowired
    private UserMapper userMapper;
    @Resource
    private Authenticator authenticator;

    @Override
    public Map<String, String> register(User user) {
        Map<String,String> error = new HashMap<>();

        if (user.getEmail() == null){
            error.put("email","邮箱不能为空");
        }else if (user.getEmail().equals("")){
            error.put("email","邮箱不能为空");
        }
        if (userMapper.findByEmail(user.getEmail()) != null){
            error.put("email","邮箱已注册");
        }

        if (user.getNickName() == null){
            error.put("nickName","呢称不能为空");
        }else if (user.getNickName().equals("")){
            error.put("nickName","呢称不能为空");
        }

        if (user.getPassword() == null){
            error.put("password","密码不能为空");
        }else if (user.getPassword().equals("")){
            error.put("password","密码不能为空");
        }

        if (user.getCode() == null){
            error.put("code","验证码不能为空");
        }else if (user.getCode().equals("")){
            error.put("code","验证码不能为空");
        }

        if (error.size() > 0) return error;

        // 验证邮箱
        if (!registerService.verifyCode(user.getEmail(), user.getCode())){
            error.put("code","邮箱验证失败");
            return error;
        }

        // 保存用户
        userMapper.saveAndFlush(user);

        return error;
    }

    @Override
    public boolean login(String email, String password) {
        User user = userMapper.findByEmailAndPassword(email,password);

        // 登录失败
        if (user == null) return false;

        // 登录成功，把用户添加到缓存
        authenticator.login(user);
        return true;
    }

    @Override
    public boolean isExist(String userId) {
        return userMapper.findByUserId(userId) != null;
    }

    @Override
    public String nickName(String userId) {
        User user = userMapper.findByUserId(userId);
        if (user == null) return "";
        return user.getNickName();
    }


}
