package com.cxc.mobile_server.Service.Impl;

import com.cxc.mobile_server.Auth.Authenticator;
import com.cxc.mobile_server.Auth.UnAuthException;
import com.cxc.mobile_server.Bean.Comment;
import com.cxc.mobile_server.Mapper.CommentMapper;
import com.cxc.mobile_server.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    private UserService userService;
    @Resource
    private ArticleService articleService;
    @Autowired
    private CommentMapper commentMapper;
    @Resource
    private PhoneService phoneService;
    @Resource
    private Authenticator authenticator;
    @Resource
    private LikingService likingService;

    @Override
    public Map<String,String> commentArticle(String userId, String toId, String content) {
        Map<String,String> error = new HashMap<>();

        if (!userService.isExist(userId)){
            error.put("用户不存在",userId);
        }
        if (!articleService.isExist(toId)){
            error.put("文章不存在",toId);
        }
        if (content.equals("")){
            error.put("评论不能为空","");
        }
        if (error.size() > 0) return error;

        saveComment(userId,null,toId,content);

        //更新缓存
        articleService.increaseComment(toId);

        return error;
    }

    @Override
    public Map<String,String> commentCom(String userId, String toUserId, String toId, String content) {
        Map<String,String> error = new HashMap<>();

        if (!userService.isExist(userId)){
            error.put("用户不存在",userId);
        }
        if (!userService.isExist(toUserId)){
            error.put("被评论的用户不存在",toUserId);
        }
        if (!isExist(toId)){
            error.put("评论 不存在",toId);
        }
        if (content.equals("")){
            error.put("评论不能为空","");
        }
        if (error.size() > 0) return error;

        saveComment(userId,toUserId,toId,content);
        return error;
    }

    @Override
    public Map<String, String> commentPhone(String userId, String toId, String content) {
        Map<String,String> error = new HashMap<>();

        if (!userService.isExist(userId)){
            error.put("用户不存在",userId);
        }
        if (!phoneService.isExist(toId)){
            error.put("评论的手机不存在",toId);
        }
        if (content.equals("")){
            error.put("评论不能为空","");
        }
        if (error.size() > 0) return error;

        saveComment(userId,null,toId,content);

        return error;
    }

    @Override
    public int commentNum(String articleId) {
        return commentMapper.countCommentByToId(articleId);
    }

    @Override
    public boolean isExist(String commentId) {
        return commentMapper.findByCommentId(commentId) != null;
    }

    @Override
    public List<Comment> artList(String articleId, int page) {
        if (!articleService.isExist(articleId)) return null;
        Pageable pageable = PageRequest.of(page-1,10);
        Page<Comment> ret = commentMapper.findByToId(articleId,pageable);
        List<Comment> list = ret.getContent();
        setSubList(list);
        return list;
    }

    @Override
    public List<Comment> phoList(String phoneId, int page) {
        if (!phoneService.isExist(phoneId)) return null;
        Pageable pageable = PageRequest.of(page-1,10);
        Page<Comment> ret = commentMapper.findByToId(phoneId,pageable);
        List<Comment> list = ret.getContent();
        setSubList(list);
        return list;
    }

    @Override
    public List<Comment> commentMeList(String userId) {
        if (!userService.isExist(userId)) return null;
        return commentMapper.findByToUserIdOrderByCreateDateDesc(userId);
    }


    /*
     * 设置评论的二级评论
     */
    private void setSubList(List<Comment> commentList){
        for (Comment comment : commentList){
            comment.setNickName(userService.nickName(comment.getUserId()));
            comment.setLikingNum(likingService.LikingNum(comment.getCommentId()));
            comment.setLiking(likingService.isLiking(getUserId(),comment.getCommentId()));
            List<Comment> comments = commentMapper.findByToId(comment.getCommentId());
            for (Comment c : comments){
                c.setNickName(userService.nickName(c.getUserId()));
                c.setLikingNum(likingService.LikingNum(c.getCommentId()));
                c.setLiking(likingService.isLiking(getUserId(),c.getCommentId()));
            }
            comment.setCommentList(comments);
        }
    }

    /*
     * 保存一条新的评论
     */
    private void saveComment(String userId, String toUserId, String toId, String content){
        Comment comment = new Comment();
        comment.setUserId(userId);
        comment.setToUserId(toUserId);
        comment.setToId(toId);
        comment.setContent(content);
        comment.setCreateDate(new Date());
        commentMapper.save(comment);
    }

    /*
     * 获取当前用户id
     */
    private String getUserId(){
        String userId = "";
        try {
            userId = authenticator.getUser().getUserId();
        } catch (UnAuthException e) {

        }
        return userId;
    }
}
