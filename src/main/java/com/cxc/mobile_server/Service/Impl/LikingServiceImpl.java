package com.cxc.mobile_server.Service.Impl;

import com.cxc.mobile_server.Bean.Liking;
import com.cxc.mobile_server.Mapper.LikingMapper;
import com.cxc.mobile_server.Service.CommentService;
import com.cxc.mobile_server.Service.LikingService;
import com.cxc.mobile_server.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LikingServiceImpl implements LikingService {

    @Resource
    private UserService userService;
    @Resource
    private CommentService commentService;
    @Autowired
    private LikingMapper likingMapper;


    @Override
    public Map<String, String> newLiking(String userId, String toUserId, String toId) {
        Map<String,String> error = new HashMap<>();
        if (!userService.isExist(userId)){
            error.put("用户不存在",userId);
        }
        if (!userService.isExist(toUserId)){
            error.put("点赞的用户不存在",toUserId);
        }
        if (!commentService.isExist(toId)){
            error.put("点赞的评论不存在",toId);
        }
        if (isLiking(userId,toId)){
            error.put("已点赞","");
        }
        if (error.size() > 0) return error;

        Liking liking = new Liking();
        liking.setUserId(userId);
        liking.setToUserId(toUserId);
        liking.setToId(toId);
        liking.setCreatedDate(new Date());
        likingMapper.saveAndFlush(liking);

        return error;
    }

    @Override
    public boolean isLiking(String userId, String toId) {
        return likingMapper.findByUserIdAndToId(userId,toId) != null;
    }

    @Override
    public List<Liking> LikingMeList(String userId) {
        if (!userService.isExist(userId)) return null;
        return likingMapper.findByToUserIdOrderByCreatedDateDesc(userId);
    }

    @Override
    public int LikingNum(String toId) {
        return likingMapper.countByToId(toId);
    }
}
