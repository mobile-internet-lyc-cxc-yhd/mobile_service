package com.cxc.mobile_server.Service;

public interface RegisterService {

    /*
     * 开始注册，发送验证码
     * email:注册邮箱
     */
    void newRegister(String email);


    /*
     * 验证邮箱
     * email:注册邮箱
     * code:验证码
     */
    boolean verifyCode(String email, String code);
}
