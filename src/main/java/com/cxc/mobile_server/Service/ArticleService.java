package com.cxc.mobile_server.Service;

import java.util.List;

public interface ArticleService {

    /*
     * 通过分页获取文章
     * pageNum: 页数
     */
    List getArticles(int pageNum);


    /*
     * 判断文章是否存在
     */
    boolean isExist(String id);

    /*
     * 增加文章评论数，用来更新缓存
     */
    void increaseComment(String id);


    /*
     * 轮播图推荐
     */
    List recommend();

}
