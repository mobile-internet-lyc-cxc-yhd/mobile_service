package com.cxc.mobile_server.Service;

import com.cxc.mobile_server.Bean.Liking;

import java.util.List;
import java.util.Map;

public interface LikingService {

    /*
     * 新建点赞
     * userId:点赞人id
     * toUserId:被点赞人id
     * toId:被点赞评论id
     */
    Map<String,String> newLiking(String userId,String toUserId,String toId);

    /*
     * 判断自己是否点赞
     * userId:用户自己的Id
     * toId:评论的Id
     */
    boolean isLiking(String userId,String toId);


    /*
     * 获取点赞我的评论的列表
     */
    List<Liking> LikingMeList(String userId);

    /*
     * 获取点赞的次数
     */
    int LikingNum(String toId);

}
