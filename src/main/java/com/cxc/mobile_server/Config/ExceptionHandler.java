package com.cxc.mobile_server.Config;

import com.cxc.mobile_server.Auth.UnAuthException;
import com.cxc.mobile_server.Bean.ReturnInfo;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(value = UnAuthException.class)
    public ReturnInfo unLogin(){
        return ReturnInfo.error("无权限");
    }

}