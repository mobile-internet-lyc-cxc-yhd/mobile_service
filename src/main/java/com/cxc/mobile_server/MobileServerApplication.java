package com.cxc.mobile_server;

import com.cxc.mobile_server.Bean.ReturnInfo;
import com.cxc.mobile_server.Service.ArticleService;
import com.cxc.mobile_server.Service.PhoneService;
import com.cxc.mobile_server.Service.RankService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@SpringBootApplication
public class MobileServerApplication {

    @Resource
    private RankService rankService;

    @Resource
    private ArticleService articleService;

    @Resource
    private PhoneService phoneService;

    public static void main(String[] args) {
        SpringApplication.run(MobileServerApplication.class, args);
    }

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public ReturnInfo test(){
        //articleService.getArticles(1);
//        phoneService.initBrand();
        return ReturnInfo.success(phoneService.getBrands());
    }

}
